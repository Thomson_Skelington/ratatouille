cmake_minimum_required(VERSION 2.8.4)
project(ratatouille)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -D_GLIBCXX_DEBUG")


FIND_PACKAGE(OpenGL)
IF(OPENGL_FOUND)
  MESSAGE(STATUS "OpenGL render API found.")
  MESSAGE(STATUS "Detected OpenGL path is : ${OPENGL_LIBRARIES}")
ENDIF(OPENGL_FOUND)

if(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
	# Workaround de mes couilles !!!!
	add_definitions(-DWIN32)
	MESSAGE(WARNING "The WIN32 constant is defined. Undefine it if you have problems. This is a workaround for the Klaus PC")
endif(${CMAKE_SYSTEM_NAME} MATCHES "Windows")

include_directories(include)

FIND_PACKAGE(GLEW)
IF(GLEW_FOUND)
  MESSAGE(STATUS "GLEW found.")
  MESSAGE(STATUS "Detected GLEW path is : ${GLEW_LIBRARIES}")
ENDIF(GLEW_FOUND)


include_directories(include source ${OPENGL_INCLUDE_DIRS})

file(GLOB_RECURSE
        source_files
        source/*
)

add_executable(ratatouille ${source_files})
if(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    set(ADDITIONAL_LIBS "mingw32")
endif()

MESSAGE(STATUS "FMOD : ${FMOD_LIBRARY}")

target_link_libraries(ratatouille m ${OPENGL_LIBRARIES} ${GLEW_LIBRARIES} ${ADDITIONAL_LIBS} SDL2main SDL2  SDL2_image fmod)
