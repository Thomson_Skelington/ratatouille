/// Vertex Generator

#version 130
uniform vec2 size;
uniform vec2 position;
uniform ivec2 window_size;
uniform float frame_offset;
uniform float frame_size;


out vec2 texCoord;		//texture coordinate in [0, 1]x[0, 1]

void main() {
	//generate base positions from vertex id: 0->(0,0); 1->(0,1); 2->(1,0); 3->(1,1); 4->(0,0);...
	texCoord = vec2(ivec2(gl_VertexID, gl_VertexID >> 1) & 1);
	gl_Position = vec4(vec2(-1.0, 1.0) + vec2(2.0, -2.0) * (texCoord * (size / vec2(window_size)) + position / vec2(window_size)), 0.0, 1.0);
	texCoord.x = texCoord.x * frame_size + frame_offset;
}

/// Textured Fragment

#version 130
in vec2 texCoord;
out vec4 color;
uniform sampler2D tex;
uniform float alpha;
uniform vec4 blended;

void main() {
	vec4 pix = texture(tex, texCoord);
	color = vec4(pix.xyz, pix.w * alpha);
}
