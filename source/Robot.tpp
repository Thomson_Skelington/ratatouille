#include <Robot.h>
#include <iostream>
#include "uptr.h"

template <class C, class... Args>
void Robot::addAction( Args &&... args) {

	auto tmp = getFinalCoordsDir();
	_moves.emplace_back(std::make_unique<C>(*this, std::forward<Args>(args)...));
	ActionPtr& a = _moves.back();

	if (!a->isValid(tmp.first)) {
        std::cout << "action invalide" << std::endl;
        _moves.pop_back();
	}

}
