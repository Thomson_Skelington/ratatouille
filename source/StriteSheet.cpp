
#include "SpriteSheet.h"

SpriteSheet::SpriteSheet(std::string filename, int nb_sprite) 
		: Texture(filename), nb_sprite(nb_sprite) {
	assert(dim.width % nb_sprite == 0);
}

int SpriteSheet::nbSprite() const {
	return nb_sprite;
}