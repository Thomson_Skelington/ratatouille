#include <iostream>
#include <actions/Move.h>
#include <actions/Wait.h>
#include "RoundConductor.h"

#include "Robot.h"
#include "Action.h"
#include "GameConductor.h"
#include "Input.h"
#include "GameData.h"
#include "SoundManager.h"

RoundConductor::RoundConductor(GameConductor &gameConductor, int roundDuration)
: _gameConductor(gameConductor)
, _doTick(&RoundConductor::tickRoundStart)
, _roundDuration(roundDuration)
, _currentAction(0)
, _actionTick(0)
, _rewinding(false)
, _finished(false)
, _currentPlayer(0)
, _showGhost(false),
_musicChannel(nullptr) { }

void RoundConductor::tick() {
	(this->*_doTick)();
}

void RoundConductor::tickRoundStart() {
	if (!_musicChannel)
		_musicChannel = gData->placementMusic.play();
	_gameConductor._robots[_currentPlayer].emplace_back(std::make_unique<Robot>(_currentPlayer));
	_current_robot = 0;
	forEachRobot(_gameConductor, [](Robot & r) {
		r.isDead = false;
	});
	_showGhost = true;
	_doTick = &RoundConductor::tickPlayersTurn;
}

void RoundConductor::tickPlayersTurn() {
	bool done = false;

	auto &robots = _gameConductor._robots[_currentPlayer];

	assert(robots.size() > 0);
	Robot & r = *robots[_current_robot];
	std::tie(_ghost_position, _ghostDir) = r.getFinalCoordsDir();
	_ghostPath = r.getPathToFinalPos();

	if (r.numActions() < roundDuration() ) {//Still have to add actions
		if (!_delayKeyPress) {
			if (Input::keyPressed(Dir::LEFT))
				r.addAction<actions::Move>(Dir::LEFT);
			else if (Input::keyPressed(Dir::RIGHT))
				r.addAction<actions::Move>(Dir::RIGHT);
			else if (Input::keyPressed(Dir::UP))
				r.addAction<actions::Move>(Dir::UP);
			else if (Input::keyPressed(Dir::DOWN))
				r.addAction<actions::Move>(Dir::DOWN);
			else if (Input::keyPressed(SDL_SCANCODE_SPACE))
				r.addAction<actions::Wait>();

			if ( Input::keyPressed(SDL_SCANCODE_LEFT)
				|| Input::keyPressed(SDL_SCANCODE_RIGHT)
				|| Input::keyPressed(SDL_SCANCODE_UP)
				|| Input::keyPressed(SDL_SCANCODE_DOWN)
				|| Input::keyPressed(SDL_SCANCODE_SPACE)) {
				_delayKeyPress = _delayKeyPressDefault;
				std::tie(_ghost_position, _ghostDir) = r.getFinalCoordsDir();
				_ghostPath = r.getPathToFinalPos();
			}
		} else --_delayKeyPress;
	} else if ((unsigned(_current_robot) + 1u)  < robots.size()) {
		_current_robot++;
	} else done = true;

	if (done) {
		++_currentPlayer;
		if (_currentPlayer < 2) {
			_doTick = &RoundConductor::tickRoundStart;
		} else {
			_actionTick = 0;
			_showGhost = false;
			Sound::stop(&this->_musicChannel);
			_musicChannel = gData->combatMusic.play();
			_doTick = &RoundConductor::tickPlayout;
		}
	}
}

void RoundConductor::tickPlayout() {
	for (; ; ) {
		if (!_actionTick)
			forEachAliveRobot(_gameConductor, [](Robot & r) {
				if (r.currentAction())
					r.currentAction()->begin();
			});

		forEachAliveRobot(_gameConductor, [this](Robot & r) {
			if (r.currentAction())
				r.currentAction()->tick(_actionTick);
		});

		if (_actionTick < Action::DURATION) {
			++_actionTick;
			return;
		}

		forEachAliveRobot(_gameConductor, [](Robot & r) {
			if (r.currentAction())
				r.currentAction()->end();
		});
		_actionTick = 0;
		++_currentAction;

		if (_currentAction == _roundDuration) {
			_actionTick = Action::DURATION;
			_currentAction = _roundDuration - 1;
			_doTick = &RoundConductor::tickWaitForRewind;
			return;
		}
	}
}

void RoundConductor::tickWaitForRewind() {
	if (Input::keyPressed(SDL_SCANCODE_SPACE)) {
		Sound::stop(&this->_musicChannel);
		_musicChannel = gData->rewindSound.play();
		_doTick = &RoundConductor::tickRewind;
	}
}

void RoundConductor::tickRewind() {
	_rewinding = true;
    forEachRobot(_gameConductor, [](Robot & r){
        r.isDead=false;
    });
	for (; ; ) {
		if (_actionTick == Action::DURATION)
			forEachRobot(_gameConductor, [](Robot & r) {
				if (r.currentAction())
					r.currentAction()->begin();
			});

		forEachRobot(_gameConductor, [this](Robot & r) {
			if (r.currentAction())
				r.currentAction()->tick(Action::DURATION - _actionTick);
		});

		if (_actionTick > 0) {
			_actionTick = std::max(_actionTick - 4, 0);
			return;
		}

		forEachRobot(_gameConductor, [](Robot & r) {
			if (r.currentAction())
				r.currentAction()->end();
		});

		_actionTick = Action::DURATION;
		--_currentAction;

		if (_currentAction < 0) {
			_currentAction = 0;
			_rewinding = false;
			_finished = true;
			Sound::stop(&this->_musicChannel);
			_musicChannel = nullptr;
			return;
		}
	}
}

bool RoundConductor::isFinished() const {
	return _finished;
}

int RoundConductor::currentPlayer() const {
	return _currentPlayer;
}

std::size_t RoundConductor::roundDuration() const {
	return _roundDuration;
}

std::size_t RoundConductor::currentAction() const {
	return _currentAction;
}

bool RoundConductor::isRewinding() const {
	return _rewinding;
}

bool RoundConductor::ghostIsShown() const {
	return _showGhost;
}

Coords RoundConductor::ghostPosition() const {
	return _ghost_position;
}

Dir RoundConductor::ghostDirection() const {
	return _ghostDir;
}

const std::vector<Coords>& RoundConductor::ghostPath() const {
	return _ghostPath;
}
