template<class T>
DynMatrix<T>::DynMatrix()
: _width(0u), _height(0u) { }

template<class T>
DynMatrix<T>::DynMatrix(std::size_t width, std::size_t height)
: _data(width * height), _width(width), _height(height) { }

template<class T>
T &DynMatrix<T>::operator ()(std::size_t x, std::size_t y) {
	return _data.at(y * _width + x);
}

template<class T>
T const &DynMatrix<T>::operator ()(std::size_t x, std::size_t y) const {
	return _data.at(y * _width + x);
}

template<class T>
void DynMatrix<T>::resize(std::size_t width, std::size_t height) {
	_width = width;
	_height = height;
	_data.resize(_width * _height);
}

template<class T>
std::vector<T> &DynMatrix<T>::data() {
	return _data;
}

template<class T>
std::vector<T> const &DynMatrix<T>::data() const {
	return _data;
}
