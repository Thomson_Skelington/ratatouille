#include "GameConductor.h"

#include "InPlace.h"
#include "RoundConductor.h"
#include <iostream>

int roundLength(int round) {
	return 20 + round * 5.0f;
}

GameConductor::GameConductor()
: _roundNumber(0)
, _map("res/bit_map_map.png") {
	_roundConductor.construct(*this, roundLength(_roundNumber));
}

void GameConductor::tick() {
	if (!_roundConductor.isConstructed())
		return;

	if (_roundConductor->isFinished()) {
		_roundConductor.destruct();
		++_roundNumber;

		if (_roundNumber < ROUNDS_NUMBER)
			_roundConductor.construct(*this, roundLength(_roundNumber));
		else {
			std::cout << "End of Game\n";
			return;
		}
	}

	_roundConductor->tick();
}

bool GameConductor::isFinished() const {
	return _roundNumber == ROUNDS_NUMBER;
}

bool GameConductor::roundOngoing() const {
	return _roundConductor.isConstructed();
}

RoundConductor &GameConductor::roundConductor() {
	return *_roundConductor;
}
