#include"Robot.h"

#include <vector>
#include <algorithm>

#include "GameData.h"
#include "GameConductor.h"

#include "uptr.h"
#include "actions/Move.h"
#include "actions/Wait.h"

#include "SpriteSheet.h"
#include "DrawQuad.h"
#include "TileSize.h"

Robot::Robot(int player)
: _player(player)
, _coords(getSpawnCoords())
, _dir(Dir::RIGHT)
, _sheet(&gData->sheets().idle[Dir::RIGHT])
, _frame(0) { }

Coords &Robot::coords() {
	return _coords;
}

Coords const &Robot::coords() const {
	return _coords;
}

Dir &Robot::dir() {
	return _dir;
}

Dir const &Robot::dir() const {
	return _dir;
}

//Action & Robot::action(size_t nb) {
//	return *_moves[nb];
//}

Action *Robot::currentAction() {
	int tmp = gData->gameConductor().roundConductor().currentAction();
	if (0 <= tmp && tmp < int(_moves.size()))
		return _moves.at(tmp).get();
	else return NULL;
}

void Robot::idle() {
	_sheet = &gData->sheets().idle[_dir];
}

void Robot::walk() {
	_sheet = &gData->sheets().walking[_dir];
}

void Robot::display() {
	displayScaledRobot(*_sheet, _frame, TILE_SIZE * coords().sum());
	_frame += 0.35f;
	if (_frame >= _sheet->nbSprite())
		_frame = 0;
}

std::pair<Coords, Dir> Robot::getFinalCoordsDir() {
	Dir tmpDir = _dir;
	Coords tmpCoords = _coords;
	_coords.pos = getSpawnCoords().pos;
	for (auto const &action : _moves) {
		action->begin();
		action->end();
	}
	Coords finalPos = _coords;
	_coords = tmpCoords;
	std::swap(_dir, tmpDir);
	return std::make_pair(finalPos, tmpDir);
}

std::vector<Coords> Robot::getPathToFinalPos() {
	std::vector<Coords> path;
	Dir tmpDir = _dir;
	Coords tmpCoords = _coords;
	_coords.pos = getSpawnCoords().pos;
	for (auto const &action : _moves) {
		path.emplace_back(_coords);
		action->begin();
		action->end();
	}
	_coords = tmpCoords;
	std::swap(_dir, tmpDir);
	return path;
}

void displayScaledRobot(SpriteSheet const &sheet, int frame, glm::vec2 pos) {
	quad->drawSprite(
		sheet,
		frame,
		pos.x + TILE_SIZE * 0.5f - sheet.spriteWidth() / 3.0f,
		pos.y - sheet.spriteWidth() * 0.75f,
		sheet.spriteWidth() * 2.0f / 3.0f,
		sheet.height() * 2.0f / 3.0f
		);
}

Coords Robot::getSpawnCoords() {
	return gData->gameConductor()._map.spawns()[_player];
}
