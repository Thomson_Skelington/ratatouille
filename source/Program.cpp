#include "Program.h"
#include <iostream>
#include <fstream>

using namespace std;

Program::Program(string srcVertex, string srcFragment)
	: program_id(compileProgram(srcVertex.c_str(), srcFragment.c_str())) {}

Program::~Program() {
	glDeleteProgram(program_id);
	GL_CHECK_ERROR;	
}

Program& Program::bind(){
	bindProgram(program_id);
	return *this;
}
Program& Program::bindIfNot(){
	bindProgramIfNot(program_id);
	return *this;
}


GLuint Program::compileProgram(const char* srcVertex, const char* srcFragment){
	
	// compile vertex shader
	GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
	GL_CHECK_ERROR;
	glShaderSource(vshader,1,&srcVertex,NULL);
	GL_CHECK_ERROR;
	glCompileShader(vshader);
	GL_CHECK_ERROR;

	// Test compilation status
	{
	int success;
	glGetShaderiv(vshader,GL_COMPILE_STATUS,&success);
	char compileLog[1024] = "\0";
	glGetShaderInfoLog(vshader,1023,NULL,compileLog);
	if (!success) {
		cerr<<compileLog<<endl;
		return 0;
	}
	else if (compileLog[0]!='\0')
		cout<<compileLog<<endl;
	}
	
	// compile fragment shader
	GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
	GL_CHECK_ERROR;
	glShaderSource(fshader,1,&srcFragment,NULL);
	GL_CHECK_ERROR;
	glCompileShader(fshader);
	GL_CHECK_ERROR;

	// Test compilation status
	{
	int success;
	glGetShaderiv(fshader,GL_COMPILE_STATUS,&success);
	char compileLog[1024] = "\0";
	glGetShaderInfoLog(fshader,1023,NULL,compileLog);
	if (!success) {
		cerr<<compileLog<<endl;
		return 0;
	}
	else if (compileLog[0]!='\0')
		cout<<compileLog<<endl;
	}

	// Create the program
	GLuint program = glCreateProgram();
	GL_CHECK_ERROR;

	// Attach the shaders to the program
	glAttachShader(program,vshader);
	GL_CHECK_ERROR;
	glAttachShader(program,fshader);
	GL_CHECK_ERROR;

	// Delete shaders (no more necessaries)
	glDeleteShader(vshader);
	GL_CHECK_ERROR;
	glDeleteShader(fshader);
	GL_CHECK_ERROR;

	// Link and validate the shader program
	glLinkProgram(program);
	GL_CHECK_ERROR;

	// Test LINK status
	{
	int success;
	glGetProgramiv(program,GL_LINK_STATUS,&success);
	if (!success){
		char compileLog[1024];
		glGetProgramInfoLog(program,1023,NULL,compileLog);
		cerr<<compileLog<<endl;
		return 0;
	}
	}
	return program;
}

string Program::getFromFile(const char* filename, string name){
	fstream file(filename);
    string shaderSource;
	string currentLine;
    assert(file && "Could not find file");
	bool shaderFound = false;
	bool lineToBeInserted = false;
	
	while(getline(file, currentLine))	//for each line of the file
	{
		if (currentLine.substr(0, 3) == "///"){	// if this line declare a shader
			if (shaderFound) break;				// if the shader has already been found before, this indicate its end.
												//	Just exit of the function.
			{									// else, parse the shader name
				const auto strBegin = currentLine.find_first_not_of(" \t/");	//trim leading spaces
				if (strBegin != std::string::npos) {								//if line is not empty
					const auto strEnd = currentLine.find_last_not_of(" \t/");	//trim trailing spaces
					const auto strRange = strEnd - strBegin + 1;				
					string currentName = currentLine.substr(strBegin, strRange);
					if (currentName == name) {									//is this shader the one we look for ?
						shaderFound = true;
						lineToBeInserted = true;
					}
				}
			}
		}
		if(lineToBeInserted) shaderSource += currentLine;
		shaderSource += "\n";
	}
	assert(shaderFound);
	return shaderSource;
}

void Program::bindProgram(GLuint program) {
	currentProgram = program;
	glUseProgram(program);
	GL_CHECK_ERROR;
}

void Program::bindProgramIfNot(GLuint program) {
	if (currentProgram == program) return;
	currentProgram = program;
	glUseProgram(program);
	GL_CHECK_ERROR;
}

GLuint Program::currentProgram = 0;
