/* 
 * File:   Sound.h
 * Author: Klaus
 *
 * Created on January 23, 2015, 11:28 PM
 */

#include <cassert>
#include "Sound.h"

FMOD_SYSTEM * Sound::s_system(NULL);

Sound::Sound(const std::string& filename, int mode):
_volume(1.0f)
,_data(NULL){
	assert(s_system != NULL);
	FMOD_RESULT result = FMOD_System_CreateSound(s_system, filename.c_str(), mode, 0, &_data);
	assert(result == FMOD_OK);
}

Sound::~Sound()
{}

void Sound::init(int nbChan){
	void * extradriverdata = 0;
	FMOD_RESULT result = FMOD_System_Create(&s_system);
	assert(result == FMOD_OK);
	result = FMOD_System_Init(s_system, nbChan, FMOD_INIT_NORMAL, extradriverdata);
	assert(result == FMOD_OK);
}
void Sound::update() {
	FMOD_System_Update(s_system);
}

FMOD_CHANNEL * Sound::play(){
	FMOD_CHANNEL * chan;
	FMOD_RESULT result = FMOD_System_PlaySound(s_system, _data, 0, false, &chan);
	assert(result == FMOD_OK);
	return chan;
}

void Sound::stop(FMOD_CHANNEL ** chan){
	if(*chan){
		FMOD_RESULT result = FMOD_Channel_Stop(*chan);
		assert(result == FMOD_OK);
	}
	*chan = nullptr;
}

FMOD_BOOL Sound::isPlaying(FMOD_CHANNEL * chan){
	FMOD_BOOL a(false);
	if(chan){
		FMOD_RESULT result = FMOD_Channel_IsPlaying(chan, &a);
		assert(result == FMOD_OK);
	}
	return a;
}

//void Sound::setVolume(float vol){
//	_volume = vol;
//	FMOD_RESULT result = FMOD_Channel_SetVolume(_chan, _volume);
//	assert(result == FMOD_OK);
//}
//
//float Sound::getVolume(){
//	return _volume;
//}
