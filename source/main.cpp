#include <iostream>
#include <algorithm>
#include <vector>

#include <SDL2/SDL.h>

#include "DrawQuad.h"
#include "GameData.h"
#include "GameOptions.h"
#include "Input.h"
#include "Sound.h"
#include "TileSize.h"

#include "SpriteAnimator.h"

#define SCREEN_MAX_FPS 60
#define SCREEN_TICKS_PER_SECOND 1000
#define SCREEN_TICKS_PER_FRAME (SCREEN_TICKS_PER_SECOND/SCREEN_MAX_FPS)
#include "DrawQuad.h"
#include "Texture.h"
#include "SpriteSheet.h"
#include "BlockMonolith.h"
#include "GraphicalEvents.h"
#include "GraphicalCounter.h"
#include <unistd.h>
#include <iostream>
#include <stdlib.h>
#include <SDL2/SDL.h>

int mainKlaus();

int mainQuentin();

int mainCedric();

int main(int argc, char* argv[]) {
	SDL_Init(SDL_INIT_EVERYTHING);

	mainQuentin();

	SDL_Quit();

	return 0;
}
int mainKlaus() {
    GameOptions options;
    options.window = {
            1024, 576, "Youhou"
    };

    gData.construct(options);
    //gData->gameConductor().map().resize(10u, 10u);

    Sound::init(32);

    /* Initialization of GLEW */
    if (glewInit() != GLEW_OK) {
        std::cout << "Unable to initialize GLEW." << std::endl;
        exit(EXIT_FAILURE);
    }

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    quad.construct(options.window.width, options.window.height);

    SDL_Event ev;
    Sound bgMusic("res/Music/David191212 - Quiet bots.mp3", FMOD_CREATESTREAM | FMOD_LOOP_NORMAL);
    bgMusic.play();

    int fps_cette_seconde = 0, fps = 0;
    Uint32 frame_start = 0, frame_time = 0, last_second_ticks = 0, current_ticks = 0;

    //BlockMonolith bmono(32);

    Intro intro;
    PlayStopSprite prout("spritesheet.png", 8, 2);
    prout.init();

    SpriteSheet cube("laserCubeAnim.png", 6);

    bool cont = true;
    do {
        current_ticks = SDL_GetTicks();
        frame_start = current_ticks;
        if (current_ticks - last_second_ticks >= SCREEN_TICKS_PER_SECOND) {
            fps = fps_cette_seconde;
            last_second_ticks = current_ticks;
            fps_cette_seconde = 0;
        }
        fps_cette_seconde++;

        Sound::update();
        Input::refresh();
        if (Input::keyPressed(SDL_SCANCODE_ESCAPE) ||
                Input::windowClosed()) {
            cont = false;
        }

        gData->gameConductor().tick();

        glClear(GL_COLOR_BUFFER_BIT);

        gData->gameConductor().map().drawBack();

        std::vector<Robot*> ySorted;

        forEachRobot(gData->gameConductor(), [&ySorted](Robot & r) {
            ySorted.push_back(&r);
        });

        std::sort(ySorted.begin(), ySorted.end(), [](Robot *lhs, Robot * rhs) {
            return lhs->coords().sum().y < rhs->coords().sum().y;
        });

        bool showGhost = gData->gameConductor().roundOngoing() && gData->gameConductor().roundConductor().ghostIsShown();

        if (showGhost) {
            for (Coords const &c : gData->gameConductor().roundConductor().ghostPath()) {
                glm::vec2 pos = TILE_SIZE * c.sum();
                quad->drawSprite(gData->sheets().cursor[0], 0, pos.x, pos.y);
            }
        }

        for (Robot *r : ySorted)
            r->display();

        if (showGhost) {
            quad->alpha = 0.75f;
            auto const &rc = gData->gameConductor().roundConductor();
            auto pos = TILE_SIZE * rc.ghostPosition().sum();
            quad->drawSprite(gData->sheets().cursor[0], 0, pos.x, pos.y);
            displayScaledRobot(gData->sheets().idle[rc.ghostDirection()], 0, pos);
            quad->alpha = 1.0f;
        }

        gData->gameConductor().map().drawFore();

        SDL_GL_SwapWindow(gData->window());

        current_ticks = SDL_GetTicks();
        frame_time = current_ticks - frame_start;
        //Max fps = SCREEN_MAX_FPS
        if (frame_time < SCREEN_TICKS_PER_FRAME)
            SDL_Delay(SCREEN_TICKS_PER_FRAME - frame_time);

        //std::cout << fps << std::endl;
    } while (cont);
    gData.destruct();
}

int mainQuentin() {

	GameOptions options;
	options.window = {
		1024, 576, "Youhou"
	};

	Sound::init(32);

	gData.construct(options);

	/* Initialization of GLEW */
	if (glewInit() != GLEW_OK) {
		std::cout << "Unable to initialize GLEW." << std::endl;
		exit(EXIT_FAILURE);
	}

	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	quad.construct(1920, 1080);

	int fps_cette_seconde = 0;
	Uint32 frame_start = 0, frame_time = 0, last_second_ticks = 0, current_ticks = 0;

	//BlockMonolith bmono(32);

	Intro intro;
	PlayStopSprite prout("spritesheet.png", 8, 2);
	prout.init();

	SpriteSheet cube("laserCubeAnim.png", 6);

	GraphicalCounter count1(12,8);
	GraphicalCounter count2(17,8);

	bool cont = true;
	do {
		current_ticks = SDL_GetTicks();
		frame_start = current_ticks;
		if (current_ticks - last_second_ticks >= SCREEN_TICKS_PER_SECOND) {
			last_second_ticks = current_ticks;
			fps_cette_seconde = 0;
		}
		fps_cette_seconde++;

		Sound::update();
		Input::refresh();
		if (Input::keyPressed(SDL_SCANCODE_ESCAPE) ||
			Input::windowClosed()) {
			cont = false;
		}

		gData->gameConductor().tick();

		glClear(GL_COLOR_BUFFER_BIT);

		gData->gameConductor().map().drawBack();

		std::vector<Robot*> ySorted;

		forEachAliveRobot(gData->gameConductor(), [&ySorted](Robot & r) {
			ySorted.push_back(&r);
		});

		std::sort(ySorted.begin(), ySorted.end(), [](Robot *lhs, Robot * rhs) {
			return lhs->coords().sum().y < rhs->coords().sum().y;
		});

		bool showGhost = gData->gameConductor().roundOngoing() && gData->gameConductor().roundConductor().ghostIsShown();

		if (showGhost) {
			auto const &rc = gData->gameConductor().roundConductor();
			for (Coords const &c : rc.ghostPath()) {
				glm::vec2 pos = TILE_SIZE * c.sum();
				quad->drawSprite(gData->sheets().cursor[rc.currentPlayer()], 0, pos.x, pos.y);
			}
		}

		for (Robot *r : ySorted)
			r->display();

		if (showGhost) {
			quad->alpha = 0.75f;
			auto const &rc = gData->gameConductor().roundConductor();
			auto pos = TILE_SIZE * rc.ghostPosition().sum();
			quad->drawSprite(gData->sheets().cursor[rc.currentPlayer()], 0, pos.x, pos.y);
			displayScaledRobot(gData->sheets().idle[rc.ghostDirection()], 0, pos);
			quad->alpha = 1.0f;
		}

		gData->gameConductor().map().drawFore();

		SDL_GL_SwapWindow(gData->window());

		current_ticks = SDL_GetTicks();
		frame_time = current_ticks - frame_start;
		//Max fps = SCREEN_MAX_FPS
		if (frame_time < SCREEN_TICKS_PER_FRAME)
			SDL_Delay(SCREEN_TICKS_PER_FRAME - frame_time);

		//std::cout << fps << std::endl;
	} while (cont);
	gData.destruct();

}



int mainCedric() {
    GameOptions options;
    options.window = {
            1024, 576, "Youhou"
    };

    gData.construct(options);
    gData->gameConductor().map().resize(10u, 10u);

    /* Initialization of GLEW */
    if (glewInit() != GLEW_OK) {
        std::cout << "Unable to initialize GLEW." << std::endl;
        exit(EXIT_FAILURE);
    }

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    quad.construct(1920, 1080);

    int fps_cette_seconde=0,fps=0;
    Uint32 frame_start=0,frame_time=0,last_second_ticks=0,current_ticks=0;


//BlockMonolith bmono(32);


    SpriteSheet cube("laserCubeAnim.png", 6);
    SpriteAnimator anim(cube,0,0,5);

    anim.playAnimation(E_Option::ONCE);

    bool cont = true;
    do {
        current_ticks = SDL_GetTicks();
        frame_start = current_ticks;
        if (current_ticks - last_second_ticks >= SCREEN_TICKS_PER_SECOND) {
            fps = fps_cette_seconde;
            last_second_ticks = current_ticks;
            fps_cette_seconde = 0;
        }
        fps_cette_seconde++;

        Input::refresh();
        if (Input::keyPressed(SDL_SCANCODE_ESCAPE) ||
                Input::windowClosed()) {
            cont = false;
        }

        gData->gameConductor().tick();

        glClear(GL_COLOR_BUFFER_BIT);

        gData->gameConductor().map().drawBack();


        anim.display(150, 200);



        std::vector<Robot*> ySorted;

        forEachRobot(gData->gameConductor(), [&ySorted](Robot & r) {
            ySorted.push_back(&r);
        });

        std::sort(ySorted.begin(), ySorted.end(), [](Robot *lhs, Robot * rhs) {
            return lhs->coords().sum().y < rhs->coords().sum().y;
        });

        for (Robot *r : ySorted)
            r->display();

        if (gData->gameConductor().roundOngoing()) {
            auto const &rc = gData->gameConductor().roundConductor();
            displayScaledRobot(gData->sheets().idle[rc.ghostDirection()], 0, TILE_SIZE * rc.ghostPosition().sum());
        }

        gData->gameConductor().map().drawFore();

        SDL_GL_SwapWindow(gData->window());

        current_ticks = SDL_GetTicks();
        frame_time = current_ticks - frame_start;
        //Max fps = SCREEN_MAX_FPS
        if (frame_time < SCREEN_TICKS_PER_FRAME)
            SDL_Delay(SCREEN_TICKS_PER_FRAME - frame_time);

        //std::cout << fps << std::endl;
    } while (cont);
    gData.destruct();

}
