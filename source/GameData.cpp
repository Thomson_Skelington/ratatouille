#include "GameData.h"

#include <cassert>

#include <SDL2/SDL_video.h>

#include "GameOptions.h"
#include "Sound.h"

glk::InPlace<GameData> gData;

GameData::GameData(GameOptions const &options)
: _window(SDL_CreateWindow(
options.window.title.c_str(),
SDL_WINDOWPOS_CENTERED,
SDL_WINDOWPOS_CENTERED,
options.window.width,
options.window.height,
SDL_WINDOW_OPENGL
))
, _glContext(SDL_GL_CreateContext(_window.get()))
, placementMusic("res/Music/David191212 - Quiet bots.mp3", FMOD_CREATESTREAM | FMOD_LOOP_NORMAL)
, combatMusic("res/Music/David191212 - Attack them.mp3", FMOD_CREATESTREAM | FMOD_LOOP_NORMAL)
, rewindSound("res/samples/reverse.wav", FMOD_CREATESTREAM | FMOD_LOOP_NORMAL) {
	_gameConductor.construct();
}

GameData::~GameData() { }

SDL_Window* GameData::window() {
	return _window.get();
}

SDL_GLContext GameData::glContext() {
	return _glContext.get();
}

GameConductor& GameData::gameConductor() {
	return *_gameConductor;
}

SpriteSheetData const &GameData::sheets() const {
	return _sheets;
}

