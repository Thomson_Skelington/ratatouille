/* 
 * File:   SoundManager.cpp
 * Author: Klaus
 * 
 * Created on January 25, 2015, 9:33 AM
 */

#include "Action.h"
#include "SoundManager.h"

std::map<std::string, Sound> SoundManager::_loadedSounds;

SoundManager::SoundManager()
{}

SoundManager::~SoundManager() {
}

Sound& SoundManager::get(const std::string& file, int flag) {
	try{
		return _loadedSounds.at(file);
	}
	catch(...){
		_loadedSounds.insert(_loadedSounds.end(), std::pair<std::string, Sound>(file, Sound(file, flag)));
		return _loadedSounds.at(file);
	}
}
