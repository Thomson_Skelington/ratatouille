
#include "SpriteAnimator.h"

using namespace std;

/*
 A optimiser :
 * verifier le resultat lorsque la duree de la frame est inferieure au nombre de frames

 */

SpriteAnimator::SpriteAnimator(const SpriteSheet & spriteSheet,float activeFrame, float currentCount, float frameRate, float animDuration):
        _spriteSheet(spriteSheet),
        _activeFrame(activeFrame),
        _currentCount(currentCount),
        _frameRate(frameRate),
        _animDuration(animDuration),
        _nbFrame(_spriteSheet.nbSprite()),
        _finish(false),
        _pause(false),
//TODO : CHANGER CETTE MERDE D'INITIALISATION DE ACTIVE ANIMATION
        _playOption(ONCE)

{
    while(_activeFrame >= _nbFrame)
        _activeFrame -= _nbFrame;
    while(_activeFrame < 0)
        _activeFrame += _nbFrame;

    if(_animDuration > 0)
        _frameRate = _animDuration / _nbFrame;
    else
        _animDuration = _nbFrame * _frameRate;

}


SpriteAnimator::~SpriteAnimator() {
}

bool SpriteAnimator::display(int x, int y, int width, int height ){
    bool around = false;
    if( (!_pause) && (!_finish))
    {
        if( ( _playOption != LOOP) && (_playOption != MIRROR_LOOP))
            _finish = ( _currentCount >= _animDuration);

        switch(_playOption){
            case ONCE:
                if(_activeFrame >= _nbFrame - 1) {
                    _finish = true;
                    _activeFrame = _nbFrame - 1;
                    break;
                }
            case LOOP:
                _activeFrame = (_currentCount / _frameRate);
                if(_activeFrame > _nbFrame) {
                    _currentCount = 0;
                    _activeFrame = 0;
                }
                break;
            case ONCE_AROUND:
            {
                unsigned int countAround = _currentCount * 2;
                if(!around){
                    _activeFrame = ( countAround  / _frameRate);
                    around = (countAround  >= _animDuration);
                    if(_activeFrame > _nbFrame) _activeFrame = 0;
                }
                else
                    _activeFrame = _nbFrame -1-((countAround  / _frameRate));

                break;
            }
            case MIRROR:
                if(_activeFrame <=0)
                    _finish = true;
            case MIRROR_LOOP:
                _activeFrame = _nbFrame - 1-(_currentCount / _frameRate);
                if(_activeFrame < 0) _activeFrame = _nbFrame - 1;
                break;
            default:
                break;
        }
        //cout<<"Active Frame :"<< _activeFrame << endl;
        ++_currentCount;


    }
    if(width = -1)
        quad->drawSprite(
                _spriteSheet,
                (int)_activeFrame,
                x,
                y
        );
    else
        quad->drawSprite(
                _spriteSheet,
                (int)_activeFrame,
                x,
                y,
                width,
                height
        );
    return _finish;
}

void SpriteAnimator::playAnimation(E_Option opt){
    _playOption = opt;
    _pause = false;
    _finish = false ;
    if(_animDuration > 0)
        _frameRate = _animDuration / (_spriteSheet.nbSprite()+1);
    _currentCount = 0;
    _pause = false;
    _finish=false;
}

bool SpriteAnimator::isFinished()const{
    return _finish;
}

void SpriteAnimator::pauseAnimation(){
    _pause = true;
}

void SpriteAnimator::stopAnimation(){
    _pause = true;
    _currentCount = 0;
}

float SpriteAnimator::getActiveFrame()const{
    return _activeFrame;
}

void SpriteAnimator::setActiveFrame(float activeFrame){
    _activeFrame = activeFrame;
    while(_activeFrame >= _nbFrame)
        _activeFrame -= _nbFrame;
    while(_activeFrame < 0)
        _activeFrame += _nbFrame;
}

float SpriteAnimator::getFrameRate()const{
    return _frameRate;
}
void SpriteAnimator::setFrameRate(float framerate){
    _frameRate = framerate;
    _animDuration = _nbFrame * _frameRate;
}

float SpriteAnimator::getAnimDuration()const{
    return _animDuration;
}
void SpriteAnimator::setAnimDuration(float animDuration){
    _animDuration = animDuration;
    _frameRate = _animDuration / _nbFrame;
}

float SpriteAnimator::getCurrentCount()const{
    return _currentCount;
}
void SpriteAnimator::setCurrentCount(float currentCount){
    _currentCount = currentCount;
}