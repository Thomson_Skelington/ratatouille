#include "DrawQuad.h"
#include "Texture.h"
#include "SpriteSheet.h"

void DrawQuad::draw(const Texture& texture, int x, int y) {
	bindIfNot();
	texture.bind();
	position.set(x, y);
	frame_offset = 0.f;
	frame_size = 1.f;
	size.set(texture.width(), texture.height());
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //generate 4 vertices
	GL_CHECK_ERROR;
}

void DrawQuad::draw(const Texture& texture, int x, int y, int width, int height) {
	bindIfNot();
	texture.bind();
	position.set(x, y);
	frame_offset = 0.f;
	frame_size = 1.f;
	size.set(width, height);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //generate 4 vertices
	GL_CHECK_ERROR;
}

void DrawQuad::drawSprite(const SpriteSheet& sprite, int frame, int x, int y) {
	bindIfNot();
	sprite.bind();
	position.set(x, y);
	size.set(sprite.spriteWidth(), sprite.height());
	frame_offset = (float) frame / sprite.nbSprite();
	frame_size = 1.f / sprite.nbSprite();
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //generate 4 vertices
	GL_CHECK_ERROR;
}

void DrawQuad::drawSprite(const SpriteSheet& sprite, int frame, int x, int y, int width, int height) {
	bindIfNot();
	sprite.bind();
	position.set(x, y);
	size.set(width, height);
	frame_offset = (float) frame / sprite.nbSprite();
	frame_size = 1.f / sprite.nbSprite();
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //generate 4 vertices
	GL_CHECK_ERROR;
}

glk::InPlace<DrawQuad> quad;