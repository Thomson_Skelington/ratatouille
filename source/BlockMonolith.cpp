#include "BlockMonolith.h"
#include "DrawQuad.h"

void BlockMonolith::addBlock(int x, int y) {
	blockPos.push_back({x, y});
}

int BlockMonolith::nextState() {
	switch(state) {
		case 0:
			addBlock(10,10);
		break;
		case 1:
			addBlock(11,9);
		break;
		case 2:
			addBlock(11,9);
		break;
		case 3:
			addBlock(11,9);
		break;
		case 4:
//			addBlock(11,9);
		break;
		default:
		break;			
	}
	return ++state;
}

void BlockMonolith::drawBlocks() const {
	for(auto block : blockPos) {
		quad->draw(illuminatedBlock, block.x * tile_size, block.y * tile_size, tile_size, tile_size);
	}
}
