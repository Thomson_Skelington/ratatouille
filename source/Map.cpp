
#include "Map.h"
#include "DrawQuad.h"
#include "GameData.h"
#include "TileSize.h"
#include <SDL2/SDL_image.h>
#include <iostream>

Map::Map(const std::string& filename) {
	SDL_Surface *image = IMG_Load(filename.c_str());
	if(!image) std::cerr <<  filename << " not found." << std::endl;
    assert(image);
	SDL_LockSurface(image);
	resize(image->w, image->h);
	int width_ = (image->w +3) & (~0x3);
	for(int y = 0 ; y < image->h ; ++y){
	for(int x = 0 ; x < image->w ; ++x){
		int indice = (y*width_ + x);
		char val = ((char*) image->pixels)[indice];
		operator()(x, y).solid = val;
	}
	}
	SDL_UnlockSurface(image);
	SDL_FreeSurface(image);
    _spawns[0].pos = {0, 12};
    _spawns[1].pos = {29, 4};
}

void Map::drawFore() const {
	quad->draw(gData->sheets().mapFront, 0,0);
}
void Map::drawBack() const {
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);	
	quad->draw(gData->sheets().mapBack, 0,0);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

