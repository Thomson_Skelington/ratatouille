/* 
 * File:   Texture.cpp
 * Author: kompil
 * 
 * Created on 23 janvier 2015, 21:58
 */

#include <GL/glew.h>

#include "Texture.h"
#include "SDL2/SDL_image.h"
#include "debug.h"

using namespace std;

Texture::Texture(string filename) {
	SDL_Surface *image = IMG_Load(filename.c_str());
	assert(image);
	SDL_LockSurface(image);
	dim.width = image->w;
	dim.height= image->h;
	glGenTextures(1, &name);
	GL_CHECK_ERROR;
	glBindTexture(GL_TEXTURE_2D, name);					//implicitly use GL_TEXTURE0 as texture unit
	GL_CHECK_ERROR;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);	//we don't use mipmap...
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);	//
	GL_CHECK_ERROR;
	switch (image->format->BitsPerPixel) {
		case 32:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, dim.width, dim.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels);
		break;
		case 24:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, dim.width, dim.height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
		break;
		default:
			assert("Wrong data format !" && false);
		break;
	}
		
	GL_CHECK_ERROR;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	//setting linear filtering...
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	//
	GL_CHECK_ERROR;
	SDL_UnlockSurface(image);
	SDL_FreeSurface(image);
}


Texture::~Texture() {
	glDeleteTextures(1, &name);
	GL_CHECK_ERROR;
}


void Texture::bind() const {
	if(boundedTexture != name) {
		boundedTexture = name;
		glBindTexture(GL_TEXTURE_2D, name);
	}
}

int Texture::boundedTexture = 0;
