#include <GameData.h>
#include "actions/Wait.h"
#include "Coords.h"
struct Robot;

namespace actions {

	Wait::Wait(Robot &subject):
            _subject(subject){ }

	ActionPtr Wait::conflict() {
		return nullptr;
	}

	void Wait::begin() { }

	void Wait::tick(int frame) {

        if(!frame)
        {

            if(!gData->gameConductor().roundConductor()._showGhost)
            {
                for(int i = 0;i < _subject.portee;++i)
                {
                    forEachAliveRobot(gData->gameConductor(), [this,&i](Robot & other){
                        if(other._player != _subject._player)
                        {
                            std::cout <<
                                    (_subject.coords().pos + i*dirToivec2[_subject.dir()]).x << ","<<
                                    (_subject.coords().pos + i*dirToivec2[_subject.dir()]).y <<  " "<<
                                    other.coords().pos.x << "," <<
                                    other.coords().pos.y << std::endl;
                            if(_subject.coords().pos + i*dirToivec2[_subject.dir()] == other.coords().pos)
                            {
                                std::cout << "PAN" << std::endl;
                                other.isDead = true;
                            }
                        }
                    });
                }
            }
        }
    }

	void Wait::end()
    {
    }

    bool Wait::isValid(Coords) {
        return true;
    }
}
