#include "actions/Move.h"
#include "Robot.h"
#include "Direction.h"
#include "GameData.h"
#include <iostream>

namespace actions {

	Move::Move(Robot &subject, Dir direction)
	: Move(subject, direction, direction) { }

	Move::Move(Robot &subject, Dir direction, Dir destDir)
	: _subject(subject)
	, _dir(direction)
	, _destDir(destDir) { }

	ActionPtr Move::conflict() {
		return nullptr;
	}

	void Move::begin() {
		_subject.dir() = _dir;
		if (gData->gameConductor().roundConductor().isRewinding()) {
			_subject.coords().pos -= dirToivec2[_dir];
			_subject.coords().offset = _totalPath = dirToivec2[_dir];
		} else {
			_subject.coords().pos += dirToivec2[_dir];
			_subject.coords().offset = _totalPath = -dirToivec2[_dir];
		}
		_subject.walk();
	}

	void Move::tick(int elapsedFrames) {
		_subject.coords().offset = (static_cast<float> (DURATION - elapsedFrames) / DURATION) * _totalPath;
	}

	void Move::end() {
		_subject.coords().offset = glm::vec2(0.0f);
		if (!gData->gameConductor().roundConductor().isRewinding())
			_subject.dir() = _destDir;
		_subject.idle();
	}

    bool Move::isValid(Coords c)
    {
        c.pos+=dirToivec2[_dir];
        return gData->gameConductor()._map.isWithinBounds(c.pos.x,c.pos.y) && !gData->gameConductor()._map(c.pos.x,c.pos.y).solid;
    }
}
