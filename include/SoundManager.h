/* 
 * File:   SoundManager.h
 * Author: Klaus
 *
 * Created on January 25, 2015, 9:33 AM
 */

#ifndef SOUNDMANAGER_H
#define	SOUNDMANAGER_H

#include <map>
#include <string>
#include "Sound.h"

class SoundManager {
	SoundManager();
public:
	~SoundManager();
	
	static Sound& get(const std::string& file, int flags = FMOD_CREATESTREAM | FMOD_LOOP_OFF);
private:
	static std::map<std::string, Sound> _loadedSounds;
};

#endif	/* SOUNDMANAGER_H */

