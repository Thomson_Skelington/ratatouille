#pragma once

#include <string>
#include <array>

#include "DynMatrix.h"
#include "Texture.h"
#include "Coords.h"

struct Tile {
	bool solid;
	int type;
} ;


class Map : public DynMatrix<Tile> {
public:
	
	Map(const std::string& level);

	void drawBack() const;
	void drawFore() const;

	std::array<Coords, 2> const &spawns() const {
		return _spawns;
	}

private:
    std::array<Coords,2> _spawns;
};
