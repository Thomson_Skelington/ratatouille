/* 
 * File:   Sound.h
 * Author: Klaus
 *
 * Created on January 23, 2015, 11:28 PM
 */

#ifndef SOUND_H
#define	SOUND_H

#ifdef WIN32
#include <windows.h>
#endif
#include <fmod.hpp>
#include <string>

class Sound{
public:
    // "mode" values are:
    // FMOD_CREATESTREAM or FMOD_CREATESAMPLE
	// FMOD_LOOP_OFF or FMOD_LOOP_NORMAL
    Sound(const std::string& filename, int mode = FMOD_CREATESTREAM | FMOD_LOOP_OFF);
    ~Sound();

    static void init(int nbChan);
	static void update();
	
    FMOD_CHANNEL * play();
	static void stop(FMOD_CHANNEL ** chan);
	static FMOD_BOOL isPlaying(FMOD_CHANNEL * chan);
//	void setVolume(float vol);
//	float getVolume();
private:
    static FMOD_SYSTEM * s_system;

    FMOD_SOUND * _data;
    float _volume;
};



#endif	/* SOUND_H */

