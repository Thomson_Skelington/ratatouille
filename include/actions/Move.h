#pragma once

#include <glm/vec2.hpp>

#include "../Action.h"
#include "../Coords.h"
#include "../Direction.h"

struct Robot;

namespace actions {

	struct Move : Action {
		Move(Robot &subject, Dir direction);
		Move(Robot &subject, Dir direction, Dir destDir);

		ActionPtr conflict() override;
		void begin() override;
		void tick(int elapsedFrames) override;
		void end() override;
        bool isValid(Coords c) override;
	private:
		Robot &_subject;
		Dir _dir;
		Dir _destDir;
		glm::vec2 _totalPath;
	} ;
}
