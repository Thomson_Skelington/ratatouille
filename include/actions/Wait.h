#pragma once

#include "../Action.h"
#include "../Robot.h"
#include "../Coords.h"

namespace actions {
////////////////////////////
////// Shooting position////
////////////////////////////
	struct Wait : Action {
		Wait(Robot &subject);

		ActionPtr conflict() override;
		void begin() override;
		void tick(int elapsedFrames) override;
		void end() override;

        virtual bool isValid(Coords coords) override;

    private :
        Robot &_subject;
    } ;
}
