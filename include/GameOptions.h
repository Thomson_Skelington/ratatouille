#pragma once

#include <string>

struct GameOptions {
	struct {
		int width, height;
		std::string title;
	} window;
};
