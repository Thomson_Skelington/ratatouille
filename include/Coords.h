#pragma once

#include <glm/vec2.hpp>

struct Coords {

	glm::vec2 sum() const {
		return glm::vec2(pos) + offset;
	}

	glm::ivec2 pos{0, 0};
	glm::vec2 offset{0.0f, 0.0f};
} ;
