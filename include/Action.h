#pragma once

#include <memory>
#include "Coords.h"

struct Action;

using ActionPtr = std::unique_ptr<Action>;

struct Action {
	static constexpr int DURATION = 30;

	// If the action cannot be performed, returns another Action to perform instead, otherwise returns nullptr.
	virtual ActionPtr conflict() = 0;

	virtual void begin() = 0;
	virtual void tick(int elapsedFrames) = 0;
	virtual void end() = 0;

    virtual bool isValid(Coords) = 0;
} ;
