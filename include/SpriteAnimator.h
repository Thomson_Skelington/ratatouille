#include <string>
#include "SpriteSheet.h"
#include "DrawQuad.h"
enum E_Option{
    ONCE,
    LOOP,
    ONCE_AROUND,
    MIRROR,
    MIRROR_LOOP
};
class SpriteAnimator {
public:
    SpriteAnimator(const SpriteSheet & spriteSheet, float activeFrame = 0, float currentCount = 0, float frameRate = 1, float _animDuration = -1);

    SpriteAnimator(const SpriteAnimator&) = default;
    virtual ~SpriteAnimator();

    bool display(int x, int y, int width = -1, int height = -1);

    void playAnimation(E_Option opt = LOOP );

    void pauseAnimation();
    void stopAnimation();

    float getActiveFrame()const;
    void setActiveFrame(float activeFrame);

    float getFrameRate()const;
    void setFrameRate(float framerate);

    float getAnimDuration()const;
    void setAnimDuration(float animDuration);

    float getCurrentCount()const;
    void setCurrentCount(float currentCount);

    void setFrameRate();

    bool isFinished()const;
private:
    const SpriteSheet &_spriteSheet;
    float _activeFrame, _frameRate, _animDuration, _currentCount, _nbFrame;
    bool _finish, _pause;
    E_Option _playOption;



};