/******************************************************************************************************/
/* This header defines a set of small classes used to represent uniform variables in shader programs. */
/* Not all types are represented. Namely matrix aren't.												  */
/* Classes are available in the template form or with OpenGL-style aliases.							  */
/* Author: Daniel Lachal																			  */
/******************************************************************************************************/

#ifndef UNIFORM_H
#define	UNIFORM_H

#include "Program.h"

/* Template form of Uniform{1|2|3|4}{f|i|ui}  with T = {float|int|unsigned int} and N = {1|2|3|4} */
template <class T = void, int N = 1> class Uniform;
/* Template form of Uniform{1|2|3|4}{f|i|ui}v with T = {float|int|unsigned int} and N = {1|2|3|4} */
template <class T = void, int N = 1> class UniformArray;

/* Base class of all uniform classes */
template<>
class Uniform<void, 1> {
protected:	
	const GLuint name; // the uniform location

public:
	/* Constructor:
	 * @param prgm the shader program to which this uniform belongs
	 * @param str_name the name of the uniform variable to query
	 */
	Uniform(const Program* prgm, const char* str_name)
		: name(glGetUniformLocation(prgm->getId(), str_name))
	{}
	GLuint getName() const { return name; }
};

/* Base class of all uniform array classes */
template<>
class UniformArray<void, 1> : public Uniform<void, 1> {
protected:	
	const unsigned int size; //number of elements in the array (fixed)

public:
	/* Constructor:
	 * @param prgm the shader program to which this uniform belongs
	 * @param str_name the name of the uniform variable to query
	 * @param size the number of elements in the array
	 */
	UniformArray(const Program* prgm, const char* str_name, unsigned int size)
		: Uniform(prgm, str_name), size(size)
	{}
	int getSize() const { return (int) size; }
};

/* These macros could be used to declare an uniform variable in a fancy way. 
 * For example, instead of declaring "Uniform2i size;" and initializing in the constructor
 * of with "MyProgram(...) : size((const Program*)this, "size") {}", 
 * just write "Uniform2i UniName(size);" in the declaration. C++0x only.
 */
#define UniName(name) name{(const Program*)this, #name}
#define UniArrayName(name, size) name{(const Program*)this, #name, size}

#define param2(param) param##_x, param##_y
#define param3(param) param##_x, param##_y, param##_z
#define param4(param) param##_r, param##_g, param##_b, param##_a
#define paramn(par,n) param##n(par)

//VS compiler does not support inherited constructors. Work around:
#define InheritConstructor(BaseName) template <class... Args> BaseName(Args&&... args) : BaseName<void, 1>(std::forward<Args>(args)...) { }

/*
 * Macro generating Uniform1{f|i|ui} classes.
 * These ones are to be generated separately to allow specific syntax with operator=.
 */
#define SingleUniform1(Type, short_type)				\
template<>												\
class Uniform<Type, 1> : public Uniform<void, 1> {		\
public:													\
/*	using Uniform<void, 1>::Uniform;  */				\
	InheritConstructor(Uniform)							\
														\
	Type operator=(Type val) { set(val); return val; }	\
	void set(Type val) {								\
		glUniform1##short_type(name, val);				\
		GL_CHECK_ERROR;									\
	}													\
};														\
using Uniform1##short_type = Uniform<Type, 1>;

/*
 * Generation of Uniform1{f|i|ui} classes.
 */
SingleUniform1(float, f)
SingleUniform1(int, i)
SingleUniform1(unsigned int, ui)

#undef SingleUniform1		//clean macro namespace

/*
 * Macro generating Uniform{2|3|4}{f|i|ui} classes.
 */
#define SingleUniformN(Type, short_type, N)				\
template<>												\
class Uniform<Type, N> : public Uniform<void, 1> {		\
public:													\
/*	using Uniform<void, 1>::Uniform;  */				\
	InheritConstructor(Uniform)							\
														\
	const Type* operator=(const Type* ptr) {			\
		set(ptr); return ptr;							\
	}													\
	void set(const Type* ptr) {							\
		glUniform##N##short_type##v(name, 1, ptr);		\
		GL_CHECK_ERROR;									\
	}													\
	void set(paramn(Type val, N)) {						\
		glUniform##N##short_type(name, paramn(val, N));	\
		GL_CHECK_ERROR;									\
	}													\
};														\
using Uniform##N##short_type = Uniform<Type, N>;

/*
 * Generation of Uniform{2|3|4}{f|i|ui} classes.
 */
SingleUniformN(float, f, 2)
SingleUniformN(int, i, 2)
SingleUniformN(unsigned int, ui, 2)
SingleUniformN(float, f, 3)
SingleUniformN(int, i, 3)
SingleUniformN(unsigned int, ui, 3)
SingleUniformN(float, f, 4)
SingleUniformN(int, i, 4)
SingleUniformN(unsigned int, ui, 4)

//clean macro namespace...
#undef SingleUniformN

#undef paramn
#undef param2
#undef param3
#undef param4

//Uniform array variables are not used in this project...
/*
#define UniformArrayN(Type, short_type, N)					\
template<>													\
class UniformArray<Type, N> : public UniformArray<void, 1> {\
public:														\
	using UniformArray<void, 1>::UniformArray;	 			\
//	InheritConstructor(UniformArray)						\
															\
	const Type* operator=(const Type* ptr) {				\
		set(ptr); return ptr;								\
	}														\
	void set(const Type* ptr) {								\
		glUniform##N##short_type##v(name, size, ptr);		\
		GL_CHECK_ERROR;										\
	}														\
};															\
using Uniform##N##short_type##v = UniformArray<Type, N>;	\
using Uniform##N##short_type##Array = UniformArray<Type, N>;

UniformArrayN(float, f, 1)
UniformArrayN(int, i, 1)
UniformArrayN(unsigned int, ui, 1)
UniformArrayN(float, f, 2)
UniformArrayN(int, i, 2)
UniformArrayN(unsigned int, ui, 2)
UniformArrayN(float, f, 3)
UniformArrayN(int, i, 3)
UniformArrayN(unsigned int, ui, 3)
UniformArrayN(float, f, 4)
UniformArrayN(int, i, 4)
UniformArrayN(unsigned int, ui, 4)

#undef UniformArrayN*/
#undef InheritConstructor

#endif	/* UNIFORM_H */

