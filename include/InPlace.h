#pragma once

#include <new>
#include <cassert>
#include <utility>

namespace glk {

	/**
	 * Espace de stockage pour une instance de T, qui peut être construite et détruite à tout moment.
	 */
	template <typename T>
	struct InPlace {

		InPlace() : _constructed(false) { }

		~InPlace() {
			if (_constructed)
				destruct();
		}

		/**
		 * @return si le T est construit
		 */
		bool isConstructed() const {
			return _constructed;
		}

		/**
		 * Construit le T
		 * @param args paramètres transmis au constructeur de T
		 * @return le T construit
		 */
		template <typename... Args>
		InPlace &construct(Args &&... args) {
			assert(("Tried to construct object twice", !_constructed));
			new (_storage) T(std::forward<Args>(args)...);
			_constructed = true;
			return *this;
		}

		/**
		 * @return le T
		 */
		T &operator *() {
			return *operator ->();
		}

		/**
		 * @return le T
		 */
		T *operator ->() {
			assert(("Object was not constructed yet", _constructed));
			return reinterpret_cast<T*> (_storage);
		}

		/**
		 * Détruit le T
		 */
		InPlace &destruct() {
			assert(("Tried to destruct non-constructed object", _constructed));
			reinterpret_cast<T*> (_storage)->~T();
			_constructed = false;
			return *this;
		}
	private:
		alignas(T) char _storage[sizeof (T)];
		bool _constructed;
	} ;
}
