#pragma once

#include "InPlace.h"
#include "RoundConductor.h"
#include "Map.h"

struct GameConductor {
	template <class F>
	friend void forEachRobot(GameConductor &c, F &&f);
	GameConductor();

	void tick();

	bool isFinished() const;

	bool roundOngoing() const;
	RoundConductor &roundConductor();

    Map &map() {
        return _map;
    }

//private:
	glk::InPlace<RoundConductor> _roundConductor;
	std::array<std::vector<std::unique_ptr<Robot>>, 2> _robots;
	int _roundNumber;
    Map _map{"res/bit_map_map.png"};

} ;

template <class F>
void forEachRobot(GameConductor &c, F &&f) {
	for (auto &player : c._robots)
		for (auto &robot : player)
			std::forward<F>(f) (*robot);
}

template <class F>
void forEachAliveRobot(GameConductor &c, F &&f) {
	for (auto &player : c._robots)
		for (auto &robot : player)
			if(!robot->isDead)
				std::forward<F>(f) (*robot);
}

int roundLength(int round);

static constexpr int const ROUNDS_NUMBER = 10;
