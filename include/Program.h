#ifndef PROGRAM_H
#define	PROGRAM_H

#include "debug.h"
#include <GL/glew.h>
#include <string>

/*
 * Wrapper class for shader program.
 */
class Program {
public:
	
	/*
	 * Construct a shader program from the code of a vertex shader and a fragment shader.
     * @param srcVertex   the code of the vertex shader
     * @param srcFragment the code of the fragment shader
     */
	Program(std::string srcVertex, std::string srcFragment);
	
	/*
	 * Clean shader program state 
	 */
	~Program();
	
	GLuint getId() const { return program_id; }
	/*
	 * Make this shader program current.
	 * @return a reference to the current Program
	 */
	Program& bind();
	/*
	 * Make this shader program current only if it is not already,
	 * because driver calls are expensive.
	 * @return a reference to the current Program
	 */
	Program& bindIfNot();
	
protected:
	const GLuint program_id; //OpenGL program name
	
public:
	

	/*
	 * Compile a shader program from the code of a vertex shader and a fragment shader.
     * @param srcVertex   the code of the vertex shader
     * @param srcFragment the code of the fragment shader
	 * @return the OpenGL program name
     */
	static GLuint compileProgram(const char* srcVertex, const char* srcFragment);
		
    /*
	 * Load one shader source code from a file that can contain multiple shaders.
 	 * Each shader is separated by a line starting with the token "///" without blank space.
 	 * This token is followed by the string name of the shader, which may include any characters.
	 * Names are case-sensitives. Leading and trailing spaces only are trimmed.
	 * @param filename the name of the file in which read the shader
	 * @param name the string name of the shader to read
	 * @return the code of the shader
	 */
	static std::string getFromFile(const char* filename, std::string name);

/* Program Manager */	
public:
	
	static GLuint currentProgram;	//The OpenGL name of the currently binded program
	
	/*
	 * Make the given shader program current.
	 * @param program the OpenGL program name, or 0 for default pipeline.
	 */
	static void bindProgram(GLuint program);
	/*
	 * Make the given shader program current is it is not already,
	 * because driver calls are expensive.
	 * @param program the OpenGL program name, or 0 for default pipeline.
	 */
	static void bindProgramIfNot(GLuint program);
	
};


#endif	/* PROGRAM_H */

