/* 
 * File:   GraphicalCounter.h
 * Author: kompil
 *
 * Created on 25 janvier 2015, 13:11
 */

#ifndef GRAPHICALCOUNTER_H
#define	GRAPHICALCOUNTER_H

#include "DrawQuad.h"
#include "GameData.h"
#include "TileSize.h"


class GraphicalCounter {
public:	
	
	GraphicalCounter(int location_x, int location_y)
	: x(TILE_SIZE*location_x), y(TILE_SIZE*location_y), count(0) {	}
	
	void setCount(int val) { count = val; }
	void incCount() { ++count; }
	
	void draw() {
		quad->draw(gData->sheets().blockVierge, x, y);
		const SpriteSheet& sheet = gData->sheets().counter;
		quad->drawSprite(sheet, count, x, y, sheet.spriteWidth(), sheet.height() * 0.85f);
	}
	
private:
	int count;
	int x,y;
};

#endif	/* GRAPHICALCOUNTER_H */

