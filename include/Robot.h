#pragma once

#include <vector>

#include "Coords.h"
#include "Action.h"
#include "Direction.h"
#include "SpriteSheet.h"
#include "InPlace.h"
#include "SpriteSheetData.h"

struct Robot {
	Robot(int player);
	Robot(Robot const &) = delete;
	Robot(Robot &&) = default;

	Coords &coords();
	Coords const &coords() const;

	Dir &dir();
	Dir const &dir() const;

	template <class C, class... Args>
	void addAction( Args &&... args);

	//Action &action(std::size_t index);
	Action *currentAction();
	std::size_t numActions(){return _moves.size();}

	void idle();
	void walk();

	void display();
    std::pair<Coords,Dir> getFinalCoordsDir();

    std::vector<Coords> getPathToFinalPos();

    Dir const &getDir() const {
        return _dir;
    }

    Coords getSpawnCoords();

    bool isDead=false;
    int _player;

    int portee=5;
	Coords _coords;
	Dir _dir;
	SpriteSheet const *_sheet;
	float _frame;
	std::vector<ActionPtr> _moves;
} ;

void displayScaledRobot(SpriteSheet const &sheet, int frame, glm::vec2 pos);

#include "Robot.tpp"
