/* 
 * File:   SpriteSheet.h
 * Author: kompil
 *
 * Created on 24 janvier 2015, 13:56
 */

#ifndef SPRITESHEET_H
#define	SPRITESHEET_H

#include "Texture.h"


class SpriteSheet : public Texture {
public:	
	SpriteSheet(std::string filename, int nb_sprite);
	
	int nbSprite() const;

	int spriteWidth() const { return dim.width / nb_sprite; }
//	int spriteHeight()const { return dim.height/ nb_sprite; }

private:	
	int nb_sprite;
	
};

#endif	/* SPRITESHEET_H */

