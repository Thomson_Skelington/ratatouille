#pragma once

#include <type_traits>

#include <SDL2/SDL_video.h>

#include "InPlace.h"
#include "UniquePtrMaker.h"
#include "Map.h"
#include "GameConductor.h"
#include "SpriteSheetData.h"
#include "Sound.h"

struct GameOptions;

using WindowPtr = glk::UniquePtrFor<SDL_Window, SDL_DestroyWindow>;
using GlContextPtr = glk::UniquePtrFor<typename std::remove_pointer<SDL_GLContext>::type, SDL_GL_DeleteContext>;

struct GameData {
	GameData(GameOptions const &options);
	~GameData();

	GameData(GameData const &) = delete;
	GameData &operator = (GameData const &) = delete;

	SDL_Window *window();
	SDL_GLContext glContext();

	GameConductor &gameConductor();

	SpriteSheetData const &sheets() const;

private:
	WindowPtr _window;
	GlContextPtr _glContext;

	glk::InPlace<GameConductor> _gameConductor;

	SpriteSheetData const _sheets;

public:
	Sound placementMusic, combatMusic, rewindSound;
} ;

extern glk::InPlace<GameData> gData;
