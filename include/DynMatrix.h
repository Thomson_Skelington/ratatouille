#pragma once

#include <vector>
#include <cstddef>
#include <search.h>
#include <iostream>
template <class T>
struct DynMatrix {
	DynMatrix();
	DynMatrix(std::size_t width, std::size_t height);

	DynMatrix(DynMatrix const &) = default;
	DynMatrix &operator = (DynMatrix const &) = default;

	void resize(std::size_t width, std::size_t height);

	T &operator () (std::size_t x, std::size_t y);
	T const &operator () (std::size_t x, std::size_t y) const;

	std::vector<T> &data();
	std::vector<T> const &data() const;

	size_t getWidth() const {
		return _width;
	}

	size_t getHeight() const {
		return _height;
	}

	bool isWithinBounds(int x, int y) {
        std::cout << getWidth() << "," << getHeight() << std::endl;

		return 0 <= x && unsigned(x) < getWidth() && 0 <= y && unsigned(y) <= getHeight();
	}
private:
	std::vector<T> _data;
	std::size_t _width, _height;
} ;

#include "DynMatrix.tpp"