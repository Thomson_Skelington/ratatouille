/* 
 * File:   BlockMonolith.h
 * Author: kompil
 *
 * Created on 24 janvier 2015, 17:02
 */

#ifndef BLOCKMONOLITH_H
#define	BLOCKMONOLITH_H

#include <vector>

#include "Texture.h"


class BlockMonolith {
	
	struct Pos{
		int x, y;
	};
	
	std::vector<Pos> blockPos;
	
	Texture illuminatedBlock;
	int state = 0;
	int tile_size;
	void addBlock(int x, int y);
	
public:
	
	BlockMonolith(int tileSize);
	
	int nextState();
	
	void drawBlocks() const;
	
	
};


#endif	/* BLOCKMONOLITH_H */

