/* 
 * File:   GraphicalEvents.h
 * Author: kompil
 *
 * Created on 24 janvier 2015, 18:52
 */

#ifndef GRAPHICALEVENTS_H
#define	GRAPHICALEVENTS_H

#include "SpriteSheet.h"


struct Intro { 

	const float animLong;
	
	Intro() : animLong(3.f){
		splash = new SpriteSheet("splash.png", 1);
		startTick = SDL_GetTicks();
		//TODO : start sound
	}
	
	SpriteSheet* splash;
	//TODO : splashSon
	
	Uint32 startTick;
		
	void drawIntro(){
		Uint32 currTick = SDL_GetTicks();
		float elapsed = (float) (currTick - startTick) / (animLong *1000.f);
		int sprite = splash->nbSprite() * elapsed;
		if(sprite < splash->nbSprite()) {
			int w, h;
			SDL_GetWindowSize(gData->window(), &w, &h);
			quad->drawSprite(*splash, (int) (splash->nbSprite() * (animLong * 1000.f)), (w - splash->spriteWidth())/2, (h - splash->height())/2);
		}
	}
	
	~Intro() {
		delete splash;
	}

    void gameBegin(){

    }

    void gameEnd(){

    }

    void rewind(){

    }

    void QuietRound(){

    }
    void AttackRound(){

    }


};
/*/
struct SpriteEvent { 

	const float animLong;
	
	SpriteEvent(char* sprite, char* song) : animLong(3.f){
		splash = new SpriteSheet(sprite, 1);
		startTick = SDL_GetTicks();
		//TODO : start sound
	}
	
	SpriteSheet* splash;
	//TODO : splashSon
	
	Uint32 startTick;
		
	void drawIntro(){
		Uint32 currTick = SDL_GetTicks();
		float elapsed = (float) (currTick - startTick) / (animLong *1000.f);
		if(elapsed < 1.f){
			int w, h;
			SDL_GetWindowSize(gData->window(), &w, &h);
			quad->drawSprite(*splash, (int) (splash->nbSprite() * (animLong * 1000.f)), (w - splash->spriteWidth())/2, (h - splash->height())/2);
		}
	}
	
	~Intro() {
		delete splash;
	}

};*/

struct PlayStopSprite : public SpriteSheet {
	
	PlayStopSprite(std::string filename, int nb_sprite, float duration) :
	SpriteSheet(filename, nb_sprite), animLong(duration) {}
	
	Uint32 startTick;
	const float animLong;
	
	void init() {
		startTick = SDL_GetTicks();
		//TODO : start sound
	}
	
	void draw(int x, int y){
		Uint32 currTick = SDL_GetTicks();
		float elapsed = (float) (currTick - startTick) / (animLong *1000.f);
		int sprite = nbSprite() * elapsed;
		if(sprite >= nbSprite()) sprite = nbSprite()-1;
		quad->drawSprite(*this, sprite, x, y);
	}
	
};

#endif	/* GRAPHICALEVENTS_H */

