/* 
 * File:   Texture.h
 * Author: kompil
 *
 * Created on 24 janvier 2015, 00:28
 */

#ifndef TEXTURE_H
#define	TEXTURE_H

#include "debug.h"
#include "GL/glew.h"
#include <string>

class Texture {
public:
	Texture(std::string filename);
	~Texture();
	
	GLuint getName() const { return name; }
	int width() const { return dim.width; }
	int height()const { return dim.height; }
	const int * dimension() const { return &dim.height; }
	
	void bind() const;
	
protected:
	
	GLuint name;
	struct {
		int width, height;
	} dim;
	
	static int boundedTexture;
	
};

#endif	/* TEXTURE_H */

