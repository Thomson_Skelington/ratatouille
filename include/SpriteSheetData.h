#pragma once

#include "uptr.h"
#include "SpriteSheet.h"

using DirectedSheet = std::array<SpriteSheet, 4>;

struct SpriteSheetData {
	friend struct GameData;

	DirectedSheet idle{
		{
			{"res/idleR.png", 15},
			{"res/idleU.png", 15},
			{"res/idleL.png", 15},
			{"res/idleD.png", 15}
		}
	};

	DirectedSheet walking{
		{
			{"res/walkR.png", 7},
			{"res/walkU.png", 7},
			{"res/walkL.png", 7},
			{"res/walkD.png", 7}
		}
	};

	DirectedSheet shooting{
		{
			{"res/shootR.png", 13},
			{"res/shootU.png", 10},
			{"res/shootL.png", 13},
			{"res/shootD.png", 10}
		}
	};

	SpriteSheet dead{"res/dead.png", 20};

	SpriteSheet cursor[2]{
		{"res/blueCursor.png", 1},
		{"res/redCursor.png", 1}
	};

	SpriteSheet counter {"res/sprite_compteur.png", 26};
	Texture blockVierge {"res/bloc_vierge.png"};
	Texture mapBack{"res/graphics/Static/map/MapBack.png"};
	Texture mapFront{"res/graphics/Static/map/MapFront.png"};

private:
	SpriteSheetData() = default;
} ;
