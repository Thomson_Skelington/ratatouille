#ifndef INPUT_H
#define	INPUT_H

#include <SDL2/SDL_events.h>

class Input {
	Input();
public:
	~Input();

	static void refresh();
	static bool keyPressed(SDL_Scancode key);
	static bool keyPressed(Dir dir);
	static bool btnPressed(unsigned char btn);
	static bool mouseMoved();
	static void getMousePos(int * x, int * y);
	static void getMouseRelPos(int * x, int * y);
	static bool windowClosed();
private:
	static Input s_instance;
	SDL_Event m_event;
	
	bool m_winClosed;
	bool m_keybd[SDL_NUM_SCANCODES];
	bool m_mouseBtn[8];
	
	int m_mouseX;
	int m_mouseY;
	int m_mouseXRel;
	int m_mouseYRel;
};

#endif	/* INPUT_H */

