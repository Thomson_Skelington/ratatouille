#pragma once

#include <memory>
#include <string>
#include <cstddef>

namespace std {

	template <class C, class... Args>
	std::unique_ptr<C> make_unique(Args &&... args) {
		return unique_ptr<C>(new C(std::forward<Args>(args)...));
	}
}
