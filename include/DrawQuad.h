/*
 * File:   DrawTile.h
 * Author: kompil
 *
 * Created on 24 janvier 2015, 00:09
 */

#ifndef DRAWQUAD_H
#define	DRAWQUAD_H

#include "Program.h"
#include "Uniform.h"
#include "InPlace.h"

class Texture;
class SpriteSheet;

struct DrawQuad : public Program {
private:
	Uniform2f UniName(position);
	Uniform2f UniName(size);
	Uniform2i UniName(window_size);
	Uniform1f UniName(frame_offset);
	Uniform1f UniName(frame_size);
	Uniform1i UniName(tex);
public:
	Uniform1f UniName(alpha);
	Uniform4f UniName(blended);

	DrawQuad(int window_width, int window_height)
	: Program(getFromFile("gui.shader", "Vertex Generator"), getFromFile("gui.shader", "Textured Fragment"))
	{
		bind();
		window_size.set(window_width, window_height);
		tex = 0; //always use TEXTURE0
		alpha = 1.0f;
		blended.set(0,0,0,0);
	}

	void draw(const Texture& texture, int x, int y, int width, int height);
	void draw(const Texture& texture, int x, int y);
	void drawSprite(const SpriteSheet& sprite, int frame, int x, int y);
	void drawSprite(const SpriteSheet& sprite, int frame, int x, int y, int width, int height);
};

extern glk::InPlace<DrawQuad> quad;

#endif	/* DRAWQUAD_H */

