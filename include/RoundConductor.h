#pragma once

#include "Sound.h"
#include "Robot.h"
#include "glm/vec2.hpp"

struct GameConductor;

struct RoundConductor {
	RoundConductor(GameConductor &gameConductor, int roundLength);

	RoundConductor(RoundConductor const &) = delete;
	RoundConductor &operator = (RoundConductor const &) = delete;

	void tick();
	bool isFinished() const;

	std::size_t roundDuration() const;
	std::size_t currentAction() const;

	bool isRewinding() const;

	int currentPlayer() const;

	bool ghostIsShown() const;
	Coords ghostPosition() const;
	Dir ghostDirection() const;
	std::vector<Coords> const &ghostPath() const;
    bool _showGhost;

private:

	void tickRoundStart();
	void tickPlayersTurn();
	void tickPlayout();
	void tickRewind();
	void tickWaitForRewind();

	GameConductor &_gameConductor;
	void (RoundConductor::*_doTick)();
	int _roundDuration, _currentAction, _actionTick;
	bool _rewinding, _finished;

    /////////////PlayersTurn state machine//////////////
    int _currentPlayer, _current_robot;
    int _delayKeyPress=0;
    const int _delayKeyPressDefault = 20;

    Coords _ghost_position;
    Dir _ghostDir;
    std::vector<Coords> _ghostPath;

	FMOD_CHANNEL *_musicChannel;
} ;
