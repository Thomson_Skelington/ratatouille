#pragma once
#include <glm/vec2.hpp>

enum Dir {
	RIGHT, UP, LEFT, DOWN, NB_DIR
} ;

static glm::ivec2 dirToivec2[Dir::NB_DIR] = {
	{1, 0},
	{0, -1},
	{-1, 0},
	{0, 1}
};


